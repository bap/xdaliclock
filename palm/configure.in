# configure.in --- xdaliclock, Copyright (c) 1999, 2002 Jamie Zawinski.
#

AC_PREREQ(2.52)
AC_INIT(../numbers2/four2.xbm)
AC_CONFIG_HEADER(config.h)

echo "current directory: `pwd`"
echo "command line was: $0 $@"

def_host=m68k-palmos

AC_CANONICAL_HOST

if test "$host" = "$build"; then

  AC_MSG_RESULT()
  AC_MSG_ERROR([To build PalmOS applications, you must cross-compile.
	   Please re-run configure like this:

	   $0 --host=$def_host --build=$build $@
])
fi


# kludge to allow AC_PROG_CC to complete, despite the entry point being
# called "PilotMain" instead of "main"...  If we don't do this, all 
# compilation tests fail (at link) because PilotMain is undefined.
#
AC_CHECK_TOOL(GCC, gcc)
CC="$GCC"
save_CC="$CC"
CC="$CC -Dmain=PilotMain"
AC_PROG_CC
CC="$save_CC"

# we need to use the compiler, not /lib/cpp
#
AC_PROG_CPP
CPP="$CC -E"


# Now make sure it's the right compiler.
#
AC_MSG_CHECKING([compiler's target architecture])
if test -n "$GCC"; then
  ac_real_host=`$CC -dumpmachine 2>&-`
  AC_MSG_RESULT([$ac_real_host])
  if test -z "$ac_real_host" ; then
    AC_MSG_RESULT()
    AC_MSG_ERROR([$CC -dumpmachine failed.  See config.log.])
  elif test "$ac_real_host" != "$def_host" ; then
    AC_MSG_RESULT()
    AC_MSG_ERROR([$CC is not the PalmOS cross compiler:
                  It generates code for \`$ac_real_host',
                  not for \`$host_alias'.])
  fi
else
  AC_MSG_RESULT([no idea -- compiler isn't gcc])
fi


# Now check for the other tools we need to build PalmOS programs.
#
AC_DEFUN(AC_CHECK_IMPORTANT_TOOL, [
  AC_CHECK_TOOL([$1], [$2], [no], [$4])
  if test "$[$1]" = "no"; then
    AC_MSG_RESULT()
    AC_MSG_ERROR([need [$2] to cross-compile PalmOS programs.
                  You don't appear to have a complete PalmOS
                  development environment installed.])
  fi
 ])

AC_CHECK_IMPORTANT_TOOL(PILRC, pilrc)
AC_CHECK_IMPORTANT_TOOL(OBJRES, obj-res)
AC_CHECK_IMPORTANT_TOOL(BUILDPRC, build-prc)


if test -n "$GCC"; then
  AC_MSG_RESULT(Turning on gcc compiler warnings.)
  CC="$CC -Wall -Wstrict-prototypes -Wnested-externs"

  AC_MSG_RESULT(Turning on -O3 optimization.)
  CFLAGS="$CFLAGS -O3"
fi

# random compiler setup
AC_C_CONST
AC_C_INLINE

# stuff for Makefiles
AC_PROG_INSTALL
AC_PROG_MAKE_SET

if test \! -z "$includedir" ; then 
  INCLUDES="$INCLUDES -I$includedir"
fi

if test \! -z "$libdir" ; then
  LDFLAGS="$LDFLAGS -L$libdir"
fi


AC_CHECK_HEADERS(PalmOS.h PalmUtils.h,,
                 AC_MSG_RESULT()
                 AC_MSG_ERROR(basic PalmOS development header files not found!
	   Do you have the PalmOS SDK 4.x or newer installed?
))

AC_CACHE_CHECK([for SysTicksPerSecond],
               ac_cv_have_SysTicksPerSecond,
               [ac_cv_have_SysTicksPerSecond=no
                AC_EGREP_HEADER(SysTicksPerSecond, PalmOS.h,
                                ac_cv_have_SysTicksPerSecond=yes)])
if test "$ac_cv_have_SysTicksPerSecond" = yes ; then
  AC_DEFINE(HAVE_SYSTICKSPERSECOND)
fi

AC_CACHE_CHECK([for PrefSetAppPreferencesV10],
               ac_cv_have_PrefSetAppPreferencesV10,
               [ac_cv_have_PrefSetAppPreferencesV10=no
                AC_EGREP_HEADER(PrefSetAppPreferencesV10, PalmOS.h,
                                ac_cv_have_PrefSetAppPreferencesV10=yes)])
if test "$ac_cv_have_PrefSetAppPreferencesV10" = yes ; then
  AC_DEFINE(HAVE_PREFSETAPPPREFERENCES_20)
fi

AC_CACHE_CHECK([for AppLaunchWithCommand],
               ac_cv_have_AppLaunchWithCommand,
               [ac_cv_have_AppLaunchWithCommand=no
                AC_EGREP_HEADER(AppLaunchWithCommand, PalmOS.h,
                                ac_cv_have_AppLaunchWithCommand=yes)
                if test "$ac_cv_have_AppLaunchWithCommand" = no ; then
                  AC_EGREP_CPP(yes, [
#                               include <PalmOS.h>
#                               ifdef AppLaunchWithCommand
                                  yes
#                               endif],
                                ac_cv_have_AppLaunchWithCommand=yes)
                fi
])
if test "$ac_cv_have_AppLaunchWithCommand" = yes ; then
  AC_DEFINE(HAVE_APPLAUNCHWITHCOMMAND)
fi

###############################################################################

CREATOR_ID=Dali

AC_DEFINE_UNQUOTED(CREATOR_ID, '$CREATOR_ID')
AC_SUBST(CREATOR_ID)
AC_SUBST(INCLUDES)
AC_OUTPUT(Makefile)
