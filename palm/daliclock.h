
#define FORM_WIDTH  160
#define FORM_HEIGHT 160
#define FORM_CENTER 75

#define MainForm		1000
#define AboutForm		1001
#define SettingsForm		1002
#define RomIncompatibleAlert	1003

#define SettingsButtonDark	2001
#define SettingsButtonLight	2002

#define SettingsButton12	2003
#define SettingsButton24	2004
#define SettingsSecondsOnly     2005

#define SettingsMMDDYY		2006
#define SettingsDDMMYY		2007
#define SettingsYYMMDD		2008
#define SettingsFieldFPS	2009

#define MainMenu		3000
#define MainMenuSettings	3001
#define MainMenuAbout	        3002
