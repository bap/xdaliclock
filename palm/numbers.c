/* Dali Clock - a melting digital clock for PalmOS.
 * Copyright (c) 1991-2010 Jamie Zawinski <jwz@jwz.org>
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  No representations are made about the suitability of this
 * software for any purpose.  It is provided "as is" without express or
 * implied warranty.
 */

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include "numbers.h"

# include "zeroF.xbm"
# include "oneF.xbm"
# include "twoF.xbm"
# include "threeF.xbm"
# include "fourF.xbm"
# include "fiveF.xbm"
# include "sixF.xbm"
# include "sevenF.xbm"
# include "eightF.xbm"
# include "nineF.xbm"
# include "colonF.xbm"
# include "slashF.xbm"
FONT(F);

# include "zeroD2.xbm"
# include "oneD2.xbm"
# include "twoD2.xbm"
# include "threeD2.xbm"
# include "fourD2.xbm"
# include "fiveD2.xbm"
# include "sixD2.xbm"
# include "sevenD2.xbm"
# include "eightD2.xbm"
# include "nineD2.xbm"
# include "colonD2.xbm"
# include "slashD2.xbm"
FONT(D2);

#if 0
# include "zeroD.xbm"
# include "oneD.xbm"
# include "twoD.xbm"
# include "threeD.xbm"
# include "fourD.xbm"
# include "fiveD.xbm"
# include "sixD.xbm"
# include "sevenD.xbm"
# include "eightD.xbm"
# include "nineD.xbm"
# include "colonD.xbm"
# include "slashD.xbm"
FONT(D);
#endif

# include "zeroE.xbm"
# include "oneE.xbm"
# include "twoE.xbm"
# include "threeE.xbm"
# include "fourE.xbm"
# include "fiveE.xbm"
# include "sixE.xbm"
# include "sevenE.xbm"
# include "eightE.xbm"
# include "nineE.xbm"
# include "colonE.xbm"
# include "slashE.xbm"
FONT(E);

const struct raw_number *get_raw_number_0 (void) { return numbers_F;  }
const struct raw_number *get_raw_number_1 (void) { return numbers_D2; }
const struct raw_number *get_raw_number_2 (void) { return numbers_E;  }
const struct raw_number *get_raw_number_3 (void) { return numbers_D2; }

/* I'd like to use numbers_D (128) for get_raw_number_3, but it makes the
   data segment too large "Signed .word overflow; switch may be too large")
 */
