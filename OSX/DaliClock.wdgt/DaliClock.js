// This file was generated by Dashcode from Apple Inc.
// You may edit this file to customize your Dashboard widget.

//
// Function: load()
// Called by HTML body element's onload event when the widget is ready to start
//
//window.DaliClock = undefined;
function load()
{
    setupParts();
        if (window.DaliClock) {

          // load preferences
          window.DaliClock.setDateStyle(widget.preferenceForKey(createInstancePreferenceKey("dateStyle")) || 0);
          window.DaliClock.setTimeStyle(widget.preferenceForKey(createInstancePreferenceKey("timeStyle")) || 0);
          window.DaliClock.setHourStyle(widget.preferenceForKey(createInstancePreferenceKey("hourStyle")) || 0);

        } else {
          showError ("Dali Clock widget plugin failed to load!");
        }
}

//
// Function: remove()
// Called when the widget has been removed from the Dashboard
//
function remove()
{
    // Stop any timers to prevent CPU usage
    // Remove any preferences as needed
    // widget.setPreferenceForKey(null, createInstancePreferenceKey("your-key"));
        if (window.DaliClock) {
          window.DaliClock.hide();
        }
}

//
// Function: hide()
// Called when the widget has been hidden
//
function hide()
{
    // Stop any timers to prevent CPU usage
  if (window.DaliClock) {
    window.DaliClock.hide();
  }
}

//
// Function: show()
// Called when the widget has been shown
//
function show()
{
    // Restart any timers that were stopped on hide
  if (window.DaliClock && front.style.display != "none") {
    var x = 12;
    var y = 28;
    var w = document.width  - (x*2);
    var h = document.height - (y*2);
    window.DaliClock.setFrame(new Array (x, y, w, h));
    window.DaliClock.show();
  }
}

//
// Function: sync()
// Called when the widget has been synchronized with .Mac
//
function sync()
{
    // Retrieve any preference values that you need to be synchronized here
    // Use this for an instance key's value:
    // instancePreferenceValue = widget.preferenceForKey(null, createInstancePreferenceKey("your-key"));
    //
    // Or this for global key's value:
    // globalPreferenceValue = widget.preferenceForKey(null, "your-key");
}

//
// Function: showBack(event)
// Called when the info button is clicked to show the back of the widget
//
// event: onClick event from the info button
//
function showBack(event)
{
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToBack");
    }

    front.style.display = "none";
    back.style.display = "block";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
	
    if (window.DaliClock) {
	window.DaliClock.hide();
	
	var d = window.DaliClock.dateStyle();
	var t = window.DaliClock.timeStyle();
	var h = window.DaliClock.hourStyle();
	document.getElementById("dateStyleMMDDYY").checked = (d == 0);
	document.getElementById("dateStyleDDMMYY").checked = (d == 1);
	document.getElementById("dateStyleYYMMDD").checked = (d == 2);
	document.getElementById("timeStyleHHMMSS").checked = (t == 0);
	document.getElementById("timeStyleHHMM").checked   = (t == 1);
	document.getElementById("timeStyleSS").checked     = (t == 2);
	document.getElementById("hourStyle12").checked     = (h == 0);
	document.getElementById("hourStyle24").checked     = (h == 1);
    }
}

//
// Function: showFront(event)
// Called when the done button is clicked from the back of the widget
//
// event: onClick event from the done button
//
function showFront(event)
{
    var front = document.getElementById("front");
    var back = document.getElementById("back");

    if (window.widget) {
        widget.prepareForTransition("ToFront");
    }

    front.style.display="block";
    back.style.display="none";

    if (window.widget) {
        setTimeout('widget.performTransition();', 0);
    }
	
    if (window.DaliClock) {
	window.DaliClock.setHourStyle(document.getElementById("hourStyle24").checked ? 1 : 0);
	window.DaliClock.setTimeStyle(document.getElementById("timeStyleSS").checked ? 2 :
						   document.getElementById("timeStyleHHMM").checked ? 1 : 0);
	window.DaliClock.setDateStyle(document.getElementById("dateStyleYYMMDD").checked ? 2 :
						   document.getElementById("dateStyleDDMMYY").checked ? 1 : 0);

	widget.setPreferenceForKey(window.DaliClock.dateStyle(), createInstancePreferenceKey("dateStyle"));
	widget.setPreferenceForKey(window.DaliClock.timeStyle(), createInstancePreferenceKey("timeStyle"));
	widget.setPreferenceForKey(window.DaliClock.hourStyle(), createInstancePreferenceKey("hourStyle"));

	window.DaliClock.show();
    }
}

function showError(error) {
	var errorDiv = document.getElementById("errorDiv");
	if (errorDiv.firstChild) {
		errorDiv.removeChild(errorDiv.firstChild);
	}
	errorDiv.appendChild(document.createTextNode(error));
	errorDiv.style.display = "block";
}

if (window.widget) {
    widget.onremove = remove;
    widget.onhide = hide;
    widget.onshow = show;
    widget.onsync = sync;
}
