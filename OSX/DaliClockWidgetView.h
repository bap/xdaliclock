/* xdaliclock - a melting digital clock
 * Copyright (c) 1991-2007 Jamie Zawinski <jwz@jwz.org>
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  No representations are made about the suitability of this
 * software for any purpose.  It is provided "as is" without express or
 * implied warranty.
 */

#import <Cocoa/Cocoa.h>
#import <WebKit/WebKit.h>
#import "DaliClockView.h"

@interface DaliClockWidgetView : DaliClockView
{
  WebView *webView;
}

/* JavaScript-ready methods
 */
- (id) web_hourStyle;
- (id) web_timeStyle;
- (id) web_dateStyle;
- (id) web_cycleSpeed;
- (id) web_usesCountdownTimer;
- (id) web_countdownDate;
- (id) web_initialForegroundColor;
- (id) web_initialBackgroundColor;

- (void) web_setHourStyle:  (id)arg;
- (void) web_setTimeStyle:  (id)arg;
- (void) web_setDateStyle:  (id)arg;
- (void) web_setCycleSpeed: (id)arg;
- (void) web_setUsesCountdownTimer:     (id)arg;
- (void) web_setCountdownDate:          (id)arg;
- (void) web_setInitialForegroundColor: (id)arg;
- (void) web_setInitialBackgroundColor: (id)arg;

- (void) web_setFrame: (WebScriptObject *)frame;
- (void) web_show;
- (void) web_hide;

@end
