/* xdaliclock - a melting digital clock
 * Copyright (c) 1991-2020 Jamie Zawinski <jwz@jwz.org>
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  No representations are made about the suitability of this
 * software for any purpose.  It is provided "as is" without express or
 * implied warranty.
 */

#ifdef USE_IPHONE

# import <Foundation/Foundation.h>
# import <UIKit/UIKit.h>
# import <OpenGLES/EAGL.h>
# import <OpenGLES/ES1/gl.h>
# import <OpenGLES/ES1/glext.h>
# import <QuartzCore/QuartzCore.h>

# define NSView  UIView
# define NSRect  CGRect
# define NSSize  CGSize
# define NSColor UIColor
# define NSImage UIImage
# define colorWithCalibratedHue colorWithHue
# define NSUserDefaultsController NSUserDefaults
# define NSArchiver NSKeyedArchiver
# define NSOpenGLContext EAGLContext
# define glOrtho glOrthof

#else  /* !USE_IPHONE */

# import <Cocoa/Cocoa.h>
# import <AppKit/NSOpenGL.h>
# import <OpenGL/OpenGL.h>
# import <OpenGL/gl.h>

#endif /* !USE_IPHONE */

#import "xdaliclock.h"

typedef struct touch_data touch_data;

@interface DaliClockView : NSView
{
  dali_config config;

  NSOpenGLContext *ogl_ctx;      // OpenGL rendering context

#ifdef USE_IPHONE

  /* The OpenGL names for the framebuffer and renderbuffer used to
     render to this view. */
  GLuint gl_framebuffer, gl_renderbuffer;
  UIView *aboutBox;
  BOOL screenLocked;

# endif /* !USE_IPHONE */
}

+ (void)registerDefaults:(NSUserDefaults *)defs;
+ (void)setUserDefaultsController:(NSUserDefaultsController *)ctl;
+ (NSUserDefaultsController *)userDefaultsController;

- (id)initWithFrame:(NSRect)rect ownWindow:(BOOL)own_p;

- (void)setInitialForegroundColor:(NSColor *)c;
- (void)setInitialBackgroundColor:(NSColor *)c;

- (void)setConstrainSizes:(BOOL)constrain_p;
- (void)setAutoDate:(float)interval;
- (IBAction) aboutClick: (id)sender;

#ifdef USE_IPHONE
- (void)setScreenLocked:(BOOL)locked;
#endif /* USE_IPHONE */


// Start timers
- (void)clockTick;
- (void)colorTick;
- (void)dateTick;


/* Bindable Properties:
     hourStyle
     timeStyle
     dateStyle
     cycleSpeed
     usesCountdownTimer
     usesHomeStretchTimer
     countdownDate
     initialForegroundColor
     initialBackgroundColor
*/

#ifdef USE_IPHONE
@property (nonatomic) int hourStyle;
@property (nonatomic) int timeStyle;
@property (nonatomic) int dateStyle;
@property (nonatomic) float cycleSpeed;
@property (nonatomic) int usesCountdownTimer;
@property (nonatomic) int usesHomeStretchTimer;
@property (nonatomic, retain) NSDate *countdownDate;
#endif /* USE_IPHONE */

@end
