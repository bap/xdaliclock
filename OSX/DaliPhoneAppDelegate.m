/* xdaliclock - a melting digital clock
 * Copyright (c) 1991-2018 Jamie Zawinski <jwz@jwz.org>
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  No representations are made about the suitability of this
 * software for any purpose.  It is provided "as is" without express or
 * implied warranty.
 */

#import "DaliPhoneAppDelegate.h"
#import "DaliClockView.h"

char *progname = "Dali Clock";   // digital.c wants this for error messages.


// In this modern world, the view rotates whether we like it or not.
//
@interface DaliClockViewController : UIViewController
@end

@implementation DaliClockViewController
- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
  return UIInterfaceOrientationMaskAll;
}

- (BOOL)shouldAutorotate			/* Added in iOS 6 */
{
  return YES;
}

- (void)viewWillTransitionToSize:(CGSize)size 
       withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)c
{
  NSRect f = [[self view] frame];
  f.size = size;
  [[self view] setFrame:f];  // Isn't this supposed to happen automatically???
}

@end



@implementation DaliPhoneAppDelegate

@synthesize window;
@synthesize contentView;


- (void)applicationDidFinishLaunching:(UIApplication *)application
{
  NSUserDefaults *defs = [NSUserDefaults standardUserDefaults];
  [DaliClockView registerDefaults:defs];

  [window setRootViewController: [[[DaliClockViewController alloc]
                                    init] retain]];

  contentView = [[DaliClockView alloc] initWithFrame:[window frame]];

  [window addSubview:contentView];
  [window makeKeyAndVisible];

  [[window rootViewController] setView:contentView];

  // After the window is visible, so that we get motion events.
  [contentView becomeFirstResponder];

  /* Display the date every minute-and-a-bit.  That way it doesn't show up
     on the same second each minute.
   */
  [contentView setAutoDate:67];
}


- (void)applicationWillResignActive:(UIApplication *)app
{
  [contentView setScreenLocked:YES];
}


- (void)applicationDidBecomeActive:(UIApplication *)app
{
  [contentView setScreenLocked:NO];
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
  [contentView setScreenLocked:YES];
}


- (void)dealloc {
  [contentView release];
  [window release];
  [super dealloc];
}

@end
