/* xdaliclock - a melting digital clock
 * Copyright (c) 1991-2010 Jamie Zawinski <jwz@jwz.org>
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  No representations are made about the suitability of this
 * software for any purpose.  It is provided "as is" without express or
 * implied warranty.
 */

#ifdef USE_IPHONE

# import <UIKit/UIKit.h>

int
main (int argc, char *argv[])
{
  NSAutoreleasePool * pool = [[NSAutoreleasePool alloc] init];
  int ret = UIApplicationMain (argc, argv, nil, nil);
  [pool release];
  return ret;
}

#else /* !USE_IPHONE */

# import <Cocoa/Cocoa.h>

int
main (int argc, char *argv[])
{
  return NSApplicationMain(argc,  (const char **) argv);
}

#endif /* !USE_IPHONE */
