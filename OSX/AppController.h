/* xdaliclock - a melting digital clock
 * Copyright (c) 1991-2009 Jamie Zawinski <jwz@jwz.org>
 *
 * Permission to use, copy, modify, distribute, and sell this software and its
 * documentation for any purpose is hereby granted without fee, provided that
 * the above copyright notice appear in all copies and that both that
 * copyright notice and this permission notice appear in supporting
 * documentation.  No representations are made about the suitability of this
 * software for any purpose.  It is provided "as is" without express or
 * implied warranty.
 */

#import <Cocoa/Cocoa.h>
#import "DaliClockView.h"

@interface AppController : NSObject
# if MAC_OS_X_VERSION_MAX_ALLOWED > MAC_OS_X_VERSION_10_5
   <NSWindowDelegate>
# endif
{
  NSWindow *window;
  DaliClockView *view;
  BOOL titlebar_p;
  BOOL fullscreen_p;
  long fullscreen_window_level;
}

- (IBAction)performFullscreen:(id)sender;
- (IBAction)performMiniaturize:(id)sender;
- (IBAction)aboutClick:(id)sender;

@end
